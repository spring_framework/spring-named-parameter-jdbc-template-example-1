package org.honor.springjdbc;

import java.util.List;

import org.honor.springjdbc.dao.OrganizationDao;
import org.honor.springjdbc.domain.Organization;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;

@Component
public class NamedParameterJdbcTemplateClassicApp1 {

	@Autowired
	private OrganizationDao dao;

	@Autowired
	private DaoUtils daoUtils;

	public void actionMethod() {
		daoUtils.createSeedData(dao);

		List<Organization> orgs = dao.getAllOrganizations();
		daoUtils.printOrganizations(orgs, daoUtils.readOperation);

		Organization org = new Organization("General Electiric", 1978, "434343", 5455, "imagine");
		boolean isCreated = dao.create(org);
		daoUtils.printSuccessFailure(daoUtils.createOperation, isCreated);
		daoUtils.printOrganizationCount(dao.getAllOrganizations(), daoUtils.createOperation);
		daoUtils.printOrganizations(dao.getAllOrganizations(), daoUtils.createOperation);

		Organization org2 = dao.getOrganization(1);
		daoUtils.printOrganization(org2, "getOrganization");

		Organization org3 = dao.getOrganization(2);
		org3.setSlogan("we imagine we do...");
		boolean isUpdated = dao.update(org3);

		boolean isDeleted = dao.delete(dao.getOrganization(3));
		daoUtils.printSuccessFailure(daoUtils.deleteOperation, isDeleted);
		daoUtils.printOrganizations(dao.getAllOrganizations(), daoUtils.deleteOperation);

		daoUtils.printSuccessFailure(daoUtils.updateOperation, isUpdated);
		daoUtils.printOrganization(dao.getOrganization(2), daoUtils.updateOperation);

		dao.cleanup();
		daoUtils.printOrganizationCount(dao.getAllOrganizations(), daoUtils.cleanupOperation);

	}

	public static void main(String[] args) {
		//create application context
		ApplicationContext ctx = new ClassPathXmlApplicationContext("beans-cp.xml");
		NamedParameterJdbcTemplateClassicApp1 mainApp = ctx.getBean(NamedParameterJdbcTemplateClassicApp1.class);
		mainApp.actionMethod();
		((ClassPathXmlApplicationContext) ctx).close();
	}

}
